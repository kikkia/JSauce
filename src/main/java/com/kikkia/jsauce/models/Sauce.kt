package com.kikkia.jsauce.models

data class Sauce(val thumbnail: String, val similarity: Double, val index: String, val externalUrl: String, val title: String)